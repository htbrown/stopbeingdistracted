const POSITIVE_MESSAGES = ["yes"]
const POSITIVE_EMOJIS = ["grin", "nerd", "party", "smile", "smile_eyes", "smile_hearts", "sunglasses"]
const NEGATIVE_MESSAGES = ["no"]
const NEGATIVE_EMOJIS = ["cry", "disappointed", "melt", "neutral", "pensive", "suspicious", "thinking"]

let update = async (currentTab) => {
    let loc = new URL(currentTab.url)
    let opts = await browser.storage.local.get()

    let host = loc.host
    let rootDomain = host.replace(/^[^.]*\.(?=\w+\.\w+$)/, "")

    document.getElementById("headingText").innerHTML = loc.hostname || "stopbeingdistracted"
    document.getElementById("toggleActivityButton").innerHTML = opts.active

    if (opts.active === true && (opts.blockedSites.includes(host) || opts.blockedSites.includes(rootDomain))) {
        document.getElementById("title").innerHTML = NEGATIVE_MESSAGES[Math.floor(Math.random() * NEGATIVE_MESSAGES.length)]
        document.getElementById("emoji").src = `../icons/emoji/negative/${NEGATIVE_EMOJIS[Math.floor(Math.random() * NEGATIVE_EMOJIS.length)]}.png`
    } else if (opts.active === false) {
        document.getElementById("title").innerHTML = "zzzz..."
        document.getElementById("emoji").src = "../icons/emoji/sleep.png"
    } else if (!host) {
        document.getElementById("title").innerHTML = "no site."
        document.getElementById("emoji").src = "../icons/emoji/negative/thinking.png"
    } else {
        document.getElementById("title").innerHTML = POSITIVE_MESSAGES[Math.floor(Math.random() * POSITIVE_MESSAGES.length)]
        document.getElementById("emoji").src = `../icons/emoji/positive/${POSITIVE_EMOJIS[Math.floor(Math.random() * POSITIVE_EMOJIS.length)]}.png`
    }
}

let activityButtonClick = async () => {
    browser.storage.local.set({
        active: !((await browser.storage.local.get()).active)
    })
    update((await browser.tabs.query({ active: true, currentWindow: true }))[0])
}

browser.storage.local.set({
    blockedSites: ["workspace.google.com"]
})

document.getElementById("toggleActivityButton").addEventListener("click", activityButtonClick)
browser.tabs.query({ active: true, currentWindow: true }).then(tabs => update(tabs[0]))
