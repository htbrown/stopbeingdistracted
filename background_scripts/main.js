browser.runtime.onInstalled.addListener(async () => {
    console.log("stopbeingdistracted initialised!")
    if (!(await browser.storage.local.get()).active) {
        browser.storage.local.set({
            active: false
        })
    }
});

browser.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
    if (changeInfo.status != "complete" || !tab.url || !tab.active) return;
    console.log(`tab ${tabId} loaded with url ${tab.url}`)

    let storage = (await browser.storage.local.get())
    if (!storage.active || storage.cooldown > new Date()) return;
    
    let host = new URL(tab.url).host
    let rootDomain = host.replace(/^[^.]*\.(?=\w+\.\w+$)/, "")
    if (!storage.blockedSites.includes(host) && !storage.blockedSites.includes(rootDomain)) return;

    browser.tabs.update(tabId, { url: browser.extension.getURL(`pages/blocked_site.html?prev=${tab.url}`), loadReplace: true })
}, { properties: ["status"] })
