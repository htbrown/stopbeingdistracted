let params = new URLSearchParams(window.location.search)

document.getElementById("goBack").onclick = () => {
    let newDate = new Date()
    newDate.setMinutes(newDate.getMinutes() + 1)
    browser.storage.local.set({
        cooldown: newDate
    })
    window.location.href = params.get("prev")
}
